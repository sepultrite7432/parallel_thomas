The program implements a modified version of Thomas Algorithm which runs on multiple cores using MPI. After division of equations on multiple cores, there are 5 steps to be carried out. 
(1) Use the modified Thomas algorithm on each core to transform the local equations such that each equation is expressed in terms of the first and last local variable. This step has no communication. 
(2) Use communication to collect the first and last local equation in each core to form a reduced tridiagonal system of equations on a particular core. This step involves communication. 
(3) Solve this reduced tridiagonal system of linear equations using the normal (serial) Thomas algorithm. No communication is required.
(4) Communicate the solution of the reduced system of equations to the respective cores i.e. solution of first 2 variables go to core 0, solution of next 2 variables go to core 1 ... the solution of the first and last equations contributed by each core are conveyed back to them. This step involves commuication. 
(5) Use these solutions of first and last equations on each core to solve for the reamining variables locally. This step requires no communication. 


