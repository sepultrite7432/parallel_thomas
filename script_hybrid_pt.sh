#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=2
#SBATCH -t 04:00:00
#SBATCH --cpus-per-task=24
#SBATCH -o output-%j
#SBATCH -e error-%j
#SBATCH --exclusive

# Normal execution
# mpiexec ./PT.exe

# for DDT debugging 
# ddt --connect mpiexec ./PT.exe 

# For Hybrid execution 
export OMP_DISPLAY_ENV=true
export OMP_NUM_THREADS=24
export OMP_PROC_BIND=spread
export OMP_PLACES=threads

mpiexec --map-by ppr:1:socket:pe=24  --report-bindings ./HPT.exe


